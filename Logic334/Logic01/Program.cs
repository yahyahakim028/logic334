﻿
//Konversi();
//OperatorAritmatika();
//Modulus();
//OperatorPenugasan();
//OperatorPerbandingan();
//OperatorLogika();
MethodReturnType();


Console.ReadKey();


static void MethodReturnType()
{
    Console.WriteLine("--Cetak Method Return Type--");
    Console.Write("Masukkan mangga : ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan apel : ");
    int apel = int.Parse(Console.ReadLine());

    int jumlah = hasil(mangga, apel);

    Console.WriteLine($"Hasil mangga + apel = {jumlah}");
}

static int hasil(int mangga, int apel)
{
    int hasil = 0;

    hasil = mangga + apel;

    return hasil;
}

static void OperatorLogika()
{
    Console.WriteLine("--Operator Logika--");
    Console.Write("Enter Your age : ");
    int age = int.Parse(Console.ReadLine());
    Console.Write("Password : ");
    string password = Console.ReadLine();

    bool isAdult = age > 18;
    bool isPasswordValid = password == "admin";

    if(isAdult && isPasswordValid)
    {
        Console.WriteLine("WELCOME TO THE JUNGLE!");
    }
    else
    {
        Console.WriteLine("SORRY, TRY AGAIN!");
    }

}

static void OperatorPerbandingan()
{
    int mangga, apel = 0;

    Console.WriteLine("--Operator Perbandingan--");
    Console.Write("Masukkan mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan apel : ");
    apel = int.Parse(Console.ReadLine());

    Console.WriteLine("Hasil Perbandingan : ");
    Console.WriteLine($"Mangga > Apel : {mangga > apel}");
    Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
    Console.WriteLine($"Mangga < Apel : {mangga < apel}");
    Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
    Console.WriteLine($"Mangga == Apel : {mangga == apel}");
    Console.WriteLine($"Mangga != Apel : {mangga != apel}");

}

static void OperatorPenugasan()
{
    int mangga = 10;
    int apel = 8;

    //isi ulang variable
    mangga = 15;

    Console.WriteLine("--Operator Penugasan--");
    Console.Write("Masukkan mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.WriteLine($"Mangga : {mangga}");
    Console.Write("Masukkan apel : ");
    apel = int.Parse(Console.ReadLine());

    //operator penugasan
    apel += 6;

    Console.WriteLine($"Apel += 6 : {apel}");
}

static void Modulus()
{
    int mangga, orang, hasil = 0;

    Console.WriteLine("--Operator Aritmatika--");
    Console.Write("Masukkan mangga : ");
    //mangga = Convert.ToInt32(Console.ReadLine()); //bisa pakai Convert atau Parse
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan orang : ");
    orang = int.Parse(Console.ReadLine());

    hasil = mangga % orang;

    Console.WriteLine($"Hasil mangga % orang = {hasil}");

}

static void OperatorAritmatika()
{
    int mangga, apel, hasil = 0;

    Console.WriteLine("--Operator Aritmatika--");
    Console.Write("Masukkan mangga : ");
    //mangga = Convert.ToInt32(Console.ReadLine()); //bisa pakai Convert atau Parse
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan apel : ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga + apel;

    Console.WriteLine($"Hasil mangga + apel = {hasil}");

}

//Fungsi/method pakai ()
static void Konversi()
{
    Console.WriteLine("--Konversi--");

    int myInt = 10;
    double myDouble = 52.25;
    bool myBool = false;

    string strMyInt = Convert.ToString(myInt);
    string strMyInt2 = myInt.ToString();
    double doubleMyInt = Convert.ToDouble(myInt);
    int intMyDouble = Convert.ToInt32(myDouble);


    Console.WriteLine(strMyInt);
    Console.WriteLine(strMyInt2);
    Console.WriteLine(doubleMyInt);
    Console.WriteLine(intMyDouble);
    Console.WriteLine(myBool.ToString());

}

