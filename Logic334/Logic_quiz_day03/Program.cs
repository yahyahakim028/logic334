﻿//SoalPR_1();
//SoalPR_2();
//SoalPR_3();
//SoalPR_4();
//SoalPR_5();
//SoalPR_6();
//SoalPR_7();
//SoalPR_8();

Console.ReadKey();


static void SoalPR_8()
{
    Console.WriteLine("--Soal PR 8 (Nilai MTK)--");
    Console.Write("Masukkan Nilai MTK : ");
    double mtk = double.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai Fisika : ");
    double fisika = double.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai Kimia : ");
    double kimia = double.Parse(Console.ReadLine());

    double rataRata = (mtk + fisika + kimia) / 3d;

    Console.WriteLine($"Nilai Rata-Rata : {rataRata}");
    if (rataRata >= 50)
    {
        Console.WriteLine($"Selamat");
        Console.WriteLine($"Kamu Berhasil");
        Console.WriteLine($"Kamu Hebat");
    }
    else
    {
        Console.WriteLine($"Maaf");
        Console.WriteLine($"Kamu Gagal");
    }
}

static void SoalPR_7()
{
    Console.WriteLine("--Soal PR 7 (BMI / Body Mass Index)--");
    Console.WriteLine("Masukkan berat badan anda : ");
    double berat = double.Parse(Console.ReadLine());
    Console.WriteLine("Masukkan tinggi badan anda : ");
    double tinggi = double.Parse(Console.ReadLine());

    double bmi = hitungBMI(berat, tinggi);
    string berbadan = "";

    if (bmi >= 25)
    {
        berbadan = "Gemuk";
    }
    else if (bmi >= 18.5)
    {
        berbadan = "Langsing";
    }
    else
    {
        berbadan = "Kurus";
    }

    Console.WriteLine($"Nilai BMI anda adalah {bmi}");
    Console.WriteLine($"Anda termasuk berbadan {berbadan}");
}

static double hitungBMI(double berat, double tinggi)
{
    double hasil = 0;
    hasil = berat / Math.Pow((tinggi / 100), 2);
    return hasil;
}

static void SoalPR_6()
{
    Console.WriteLine("--Soal PR 6 (GaPok)--");
    Console.WriteLine("Masukkan Nama : ");
    string nama = Console.ReadLine();
    Console.WriteLine("Masukkan Tunjangan : ");
    double tunjangan = double.Parse(Console.ReadLine());
    Console.WriteLine("Masukkan Gaji Pokok : ");
    double gapok = double.Parse(Console.ReadLine());
    Console.WriteLine("Masukkan Banyaknya Bulan : ");
    double banyakBulan = double.Parse(Console.ReadLine());

    double persenPajak = (gapok + tunjangan) >= 10000000 ? 15 : (gapok + tunjangan) >= 5000000 ? 10 : (gapok + tunjangan) > 0 ? 5 : 0;

    double pajak = persenPajak / Convert.ToDouble(100) * (gapok + tunjangan);
    double bpjs = 0.03 * (gapok + tunjangan);
    double gajiPerBulan = (gapok + tunjangan) - (pajak + bpjs);
    double totalGaji = ((gapok + tunjangan) - (pajak + bpjs)) * banyakBulan;

    Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
    Console.WriteLine($"Pajak : Rp. {pajak}");
    Console.WriteLine($"BPJS : Rp. {bpjs}");
    Console.WriteLine($"Gaji/Bulan : Rp. {gajiPerBulan}");
    Console.WriteLine($"Total Gaji * Banyak Bulan : Rp. {totalGaji}");
}

static void SoalPR_5()
{
    Console.WriteLine("--Soal PR 5 (Genarasi Baby Boomer)--");
    Console.WriteLine("Masukkan nama anda : ");
    string nama = Console.ReadLine();
    Console.WriteLine("Tahun berapa anda lahir : ");
    int tahunLahir = int.Parse(Console.ReadLine());

    string generasi = "";

    if (tahunLahir >= 1944 && tahunLahir <= 1964)
    {
        generasi = "Baby Goomer";
    }
    else if (tahunLahir >= 1965 && tahunLahir <= 1979)
    {
        generasi = "Generasi X";
    }
    else if (tahunLahir >= 1980 && tahunLahir <= 1994)
    {
        generasi = "Generasi Y";
    }
    else if (tahunLahir >= 1995 && tahunLahir <= 2015)
    {
        generasi = "Generasi Z";
    }
    else
    {
        generasi = "netral (tidak tergolong generasi apapun)";
    }

    Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong {generasi}");
}

static void SoalPR_4()
{
    Console.WriteLine("--Soal PR 4 (Voucher Shopee)--");
    Console.WriteLine("Masukkan Belanja : ");
    int belanja = int.Parse(Console.ReadLine());
    Console.WriteLine("Masukkan Ongkos Kirim : ");
    int ongkir = int.Parse(Console.ReadLine());
    Console.WriteLine("Pilih Voucher : ");
    int voucher = int.Parse(Console.ReadLine());

    int diskonOngkir = 0;
    int diskonBelanja = 0;
    if (belanja >= 100000 && voucher == 3)
    {
        diskonOngkir = 20000;
        diskonBelanja = 10000;
    }
    else if (belanja >= 50000 && voucher == 2)
    {
        diskonOngkir = 10000;
        diskonBelanja = 10000;
    }
    else if (belanja >= 30000 && voucher == 1)
    {
        diskonOngkir = 5000;
        diskonBelanja = 5000;
    }

    Console.WriteLine($"Belanja : {belanja}");
    Console.WriteLine($"Ongkos Kirim : {ongkir}");
    Console.WriteLine($"Diskon Ongkir : {diskonOngkir}");
    Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
    Console.WriteLine($"Total Belanja : {belanja + ongkir - diskonOngkir - diskonBelanja}");
}

static void SoalPR_3()
{
    Console.WriteLine("--Soal PR 3 (Voucher Grab Food)--");
    Console.WriteLine("Masukkan Belanja : ");
    int belanja = int.Parse(Console.ReadLine());
    Console.WriteLine("Masukkan Jarak : ");
    int jarak = int.Parse(Console.ReadLine());
    Console.WriteLine("Masukkan Promo : ");
    string promo = Console.ReadLine();

    int ongkir = jarak <= 5 ? 5000 : jarak * 1000;
    double diskon = 0;

    if (promo.ToUpper() == "JKTOVO" && belanja >= 30000)
    {
        diskon = belanja * 0.4;
        if (diskon > 30000)
        {
            diskon = 30000;
        }
    }

    Console.WriteLine($"Belanja : {belanja}");
    Console.WriteLine($"Diskon 40% : {diskon}");
    Console.WriteLine($"Ongkir : {ongkir}");
    Console.WriteLine($"Total Belanja : {belanja + ongkir - diskon}");
}

static void SoalPR_2()
{
    Console.WriteLine("--Soal PR 2 (Pulsa)--");
    Console.WriteLine("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());

    int point = input >= 100000 ? 800 : input >= 50000 ? 400 : input >= 25000 ? 200 : input >= 10000 ? 80 : 0;

    Console.WriteLine($"Pulsa : {input}");
    Console.WriteLine($"Point : {point}");
}

static void SoalPR_1()
{
    Console.WriteLine("--Soal PR 1 (Grade)--");
    Console.WriteLine("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());

    ////CARA VALIDASI BILA DIINPUT SELAIN ANGKA
    //string nilai = "";
    //if (nilai.All(char.IsDigit)){
    //    Console.WriteLine("Harap masukkan berupa angka");
    //}

    if (input >= 90 && input <= 100)
    {
        Console.WriteLine("Anda mendapatkan nilai Grade A");
    }
    else if (input >= 70 && input <= 89)
    {
        Console.WriteLine("Anda mendapatkan nilai Grade B");
    }
    else if (input >= 50 && input <= 69)
    {
        Console.WriteLine("Anda mendapatkan nilai Grade C");
    }
    else if (input >= 0 && input < 50)
    {
        Console.WriteLine("Anda mendapatkan nilai Grade E");
    }
    else
    {
        Console.WriteLine("Anda salah memasukkan nilai");
    }
}