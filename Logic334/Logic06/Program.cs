﻿using Logic06;


//AbstactClass();
//ObjectClass();
//Constructor();
//Encapsulation();
//Inheritance();
Overriding();


Console.ReadKey();


static void Overriding()
{
    Console.WriteLine("--Overriding--");
    Kucing kucing = new Kucing();
    Paus paus = new Paus();

    Console.WriteLine($"Kucing {kucing.pindah()}");
    Console.WriteLine($"Paus {paus.pindah()}");
}

static void Inheritance()
{
    Console.WriteLine("--Enheritance--");
    TypeMobil typeMobil = new TypeMobil();
    typeMobil.Civic();
}

static void Encapsulation()
{
    Console.WriteLine("--Encapsulation--");
    PersegiPanjang pp = new PersegiPanjang();
    pp.panjang = 4.5;
    pp.lebar = 3.5;

    pp.TampilkanData();
}

static void Constructor()
{
    Console.WriteLine("--Constructor--");
    Mobil mobil = new Mobil("B 123 MX");
    //string platno = mobil.platno;
    string platno = mobil.getPlatno();

    Console.WriteLine($"Mobil dengan Nomor Polisi : {platno}");
}

static void ObjectClass()
{
    Console.WriteLine("--Object Class--");

    Mobil mobil = new Mobil() { nama = "Ferrari", kecepatan = 0, bensin = 10, posisi = 0 };

    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20);

    mobil.utama();
}

static void AbstactClass()
{
    Console.WriteLine("--Abstract Class--");
    Console.Write("Masukkan input x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan input y : ");
    int y = int.Parse(Console.ReadLine());

    TestTurunan calc = new TestTurunan();
    int jumlah = calc.jumlah(x, y);
    int kurang = calc.kurang(x, y);

    Console.WriteLine($"Hasil {x} + {y} = {jumlah}");
    Console.WriteLine($"Hasil {x} - {y} = {kurang}");
}