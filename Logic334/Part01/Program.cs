﻿
Utama();

Console.ReadKey();

static void Utama()
{
    int pilihan;
    bool ulangi = true;
    string input;
    while (ulangi)
    {
        Console.Write("Masukkan kode soal = ");
        pilihan = int.Parse(Console.ReadLine());
        switch (pilihan)
        {
            case 1:
                Soal1();
                break;
            case 2:
                Soal2();
                break;
            default:
                Console.WriteLine("Tidak ada soal yang dipilih");
                break;

        }
        Console.WriteLine($"\nUlangi Proses ? Y/N ");
        input = Console.ReadLine();
        if (input.ToUpper() == "N")
        {
            ulangi = false;
        }
        else if (input.ToUpper() == "Y")
        {
            Console.Clear();
            ulangi = true;
        }
        else
        {
            Console.WriteLine("Anda Memasukan kode yang salah");
            ulangi = false;
        }
    }
    Console.ReadKey();

}

static void Soal2()
{
    Console.WriteLine("--Soal 2--");
    Console.Write("Masukkan n (kalimat) : ");
    string n = Console.ReadLine().ToLower().Replace(" ", "");

    List<char> listVokal = new List<char>();
    List<char> listKonsonan = new List<char>();
    for (int i = 0; i < n.Length; i++)
    {
        if (n[i] == 'a' || n[i] == 'i' || n[i] == 'u' || n[i] == 'e' || n[i] == 'o')
        {
            listVokal.Add(n[i]);
        }
        else
        {
            listKonsonan.Add(n[i]);
        }
    }
    Console.WriteLine("Huruf vokal : " + String.Join("", listVokal.OrderBy(a => a)));
    Console.WriteLine("Huruf konsonan : " + String.Join("", listKonsonan.OrderBy(a => a)));
}

static void Soal1()
{
    //balik_kesini :

    Console.WriteLine("--Soal 1--");
    Console.Write("Masukkan n : ");
    int n = int.Parse(Console.ReadLine());

    List<int> listGenap = new List<int>();
    List<int> listGanjil = new List<int>();
    for(int i = 1; i <= n; i++)
    {
        if(i % 2 == 0)
        {
            listGenap.Add(i);
        }
        else
        {
            listGanjil.Add(i);
        }
    }
    Console.WriteLine(String.Join(" ", listGanjil));
    Console.WriteLine(String.Join(" ", listGenap));

    //goto balik_kesini;
}

