﻿// See https://aka.ms/new-console-template for more information

//Output
Console.WriteLine("Hello, World!");
Console.WriteLine("Hello Gaes!");
Console.Write("HELLO GAES!");
Console.WriteLine();
Console.Write("hello GAES!");
Console.WriteLine();

Console.Write("Masukkan nama : ");

//Input
string nama = Console.ReadLine();

//Variable muttable
string kelas = "Batch 334";
string Nama = "";
int umur = 19;
bool isExpired = true;

//Implisit
var nilai = 'a';//char
var nilai2 = "a";//string
var nilai3 = 10;//numeric/int
var nilai4 = false;//boolean

//Variable immutable
const double phi = 3.14;


kelas = "Batch 334 OKE";


Console.WriteLine(nama);

//penggabungan string
Console.WriteLine("Hi, {0} Selamat Datang! {1} {2} {0}", nama, kelas, nama);
Console.WriteLine("Hi, " + nama + " Selamat " + kelas + " Datang!");
Console.WriteLine($"Hi, { nama } { kelas } Selamat Datang!");



Console.ReadKey();
