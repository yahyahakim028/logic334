﻿
CaesarCipher();
//Pangram();

Console.ReadKey();

//HACKERRANK - WARMUP
//HACKERRANK - STRINGS

static void Pangram()
{
    string alphabet = "abcdefghijklmnopqrstuvwxyz" /*ABCDEFGHIJKLMNOPQRSTUVWXYZ*/;

    Console.WriteLine("Masukan Kata : ");
    string input = Console.ReadLine().ToLower();
    int hasil = 0;
    for (int i = 0; i < alphabet.Length; i++)
    {
        int b = input.IndexOf(alphabet[i]);
        if (b > -1)
        {
            hasil += 1;
        }
    }
    Console.WriteLine(hasil);
    if (hasil >= 26)
    {
        Console.WriteLine("Kalimat ini adalah Pangram");
    }
    else
        Console.WriteLine("Kalimat ini bukan Pangram");
}

static void CaesarCipher()
{
    string s = "middle-Outz";
    int k = 2;
    string alphabet = "abcdefghijklmnopqrstuvwxyz";
    string alphabetrotate = alphabet.Substring(k) + alphabet.Substring(0, k);
    //Console.Write(alphabetrotate);
    string hasil = "";
    char[] arrayAlphabet = alphabet.ToCharArray();
    char[] arrayAlphabetrotate = alphabetrotate.ToCharArray();
    char[] array = s.ToCharArray();
    for (int i = 0; i < array.Length; i++)
    {
        if (alphabet.Contains(char.ToLower(array[i])))
        {
            if (char.IsUpper(array[i]))
            {
                hasil += arrayAlphabetrotate[Array.IndexOf(arrayAlphabet, char.ToLower(array[i]))].ToString().ToUpper();
            }
            else
            {
                hasil += arrayAlphabetrotate[Array.IndexOf(arrayAlphabet, char.ToLower(array[i]))].ToString().ToLower();
            }
        }
        else
        {
            hasil += array[i].ToString();
        }
    }

    Console.WriteLine(hasil);
}
