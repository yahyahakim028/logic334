﻿
//IfStatement();
//IfElseStatement();
//IfElseIfStatement();
//IfNested();
//Ternary();
Switch();


Console.ReadKey();


static void Switch()
{
    Console.WriteLine("--Switch--");
    Console.Write("Pilih buah kesukaan Anda (apel, jeruk, pisang) :");
    string pilihan = Console.ReadLine().ToLower();

    switch (pilihan)
    {
        case "apel":
            Console.WriteLine("Anda memilih buah apel");
            break;
        case "jeruk":
            Console.WriteLine("Anda memilih buah jeruk");
            break;
        case "pisang":
            Console.WriteLine("Anda memilih buah pisang");
            break;
        default:
            Console.WriteLine("Anda memilih yang lain");
            break;
    }

}

static void Ternary()
{
    Console.WriteLine("--Ternary--");
    Console.Write("Masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai y : ");
    int y = int.Parse(Console.ReadLine());

    if(x > y)
    {
        Console.WriteLine("x lebih besar dari pada y");
    }
    else if(x < y)
    {
        Console.WriteLine("x lebih kecil dari pada y");
    }
    else
    {
        Console.WriteLine("x sama dengan y");
    }

    string z = x > y ? "x lebih besar dari pada y" : x < y ? "x lebih kecil dari pada y" : "x sama dengan y"; //cara ternary

    Console.WriteLine(z);

}

static void IfNested()
{
    Console.WriteLine("--If Nested--");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    if(nilai >= 50)
    {
        Console.WriteLine("Kamu Berhasil");

        if(nilai == 100)
        {
            Console.WriteLine("Kamu Keren");
        }
    }
    else
    {
        Console.WriteLine("Kamu Gagal");
    }

}

static void IfElseIfStatement()
{
    Console.WriteLine("--If Else If Statement--");
    Console.Write("Masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());

    if(x == 10)
    {
        Console.WriteLine("x value equals to 10");
    }
    else if(x > 10)
    {
        Console.WriteLine("x value greather than 10");
    }
    else
    {
        Console.WriteLine("x value less than 10");
    }

}

static void IfElseStatement()
{
    Console.WriteLine("--If Else Statement--");
    Console.Write("Masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());

    if(x >= 10)
    {
        Console.WriteLine("x is greater than or equals 10");
    }
    else
    {
        Console.WriteLine("x is less than 10");
    }

}

static void IfStatement()
{
    Console.WriteLine("--If Statement--");
    Console.Write("Masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai y : ");
    int y = int.Parse(Console.ReadLine());

    if(x >= 10)
    {
        Console.WriteLine("x is greater than or equals 10");
    }

    if(y <= 5)
    {
        Console.WriteLine("y is lesser than or equals 5");
    }

}