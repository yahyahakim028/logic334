﻿
Tugas1();
//Tugas2();
//Tugas3();
//Tugas4();
//Tugas5();
//Tugas6();

//Tugas dari LMS 1 Dimm Nomor 1 s/d 5
//Tugas_1_to_4();
//Tugas_5();


Console.ReadKey();


static void Tugas_5()
{
}

static void Tugas_1_to_4()
{ 
}

static void Tugas6()
{
    Console.WriteLine("--Sorting--");
    Console.Write("Masukkan input (pakai koma) : ");
    int[] arrTampung = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

    int temp = 0;
    for (int i = 0; i < arrTampung.Length - 1; i++)
    {
        for (int j = 0; j < arrTampung.Length - 1; j++)
        {
            if (arrTampung[j] > arrTampung[j + 1])
            {
                temp = arrTampung[j + 1];
                arrTampung[j + 1] = arrTampung[j];
                arrTampung[j] = temp;
            }
        }
    }

    Console.WriteLine(string.Join(",", arrTampung));
}

static void Tugas5()
{
    Console.WriteLine("--Change Position (Array)--");
    Console.Write("Masukkan input (pakai koma) : ");
    int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
    Console.Write("Masukkan banyak rotasi : ");
    int rotasi = int.Parse(Console.ReadLine());

    int arr0 = 0;
    for (int r = 0; r < rotasi; r++)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (i < array.Length - 1)
            {
                if (i == 0)
                {
                    arr0 = array[i];
                }
                array[i] = array[i + 1];
            }
            else
            {
                array[i] = arr0;
            }
        }
    }

    Console.WriteLine(String.Join(",", array));
}

static void Tugas4()
{
    Console.WriteLine("--Belanja Lebaran--");
    Console.Write("Masukkan Uang Andi : ");
    int uang = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Harga Baju (pakai koma) : ");
    int[] hargaBajuArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
    Console.Write("Masukkan Harga Celana (pakai koma) : ");
    int[] hargaCelanaArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

    int maxBelanja = 0;
    for (int i = 0; i < hargaBajuArray.Length; i++)
    {
        for (int j = 0; j < hargaCelanaArray.Length; j++)
        {
            int harga = hargaBajuArray[i] + hargaCelanaArray[j];
            if (harga <= uang && harga >= maxBelanja)
            {
                maxBelanja = harga;
            }
        }
    }

    Console.WriteLine($"Maksimal Uang yang dibelanjakan Andi : {maxBelanja}");
}

static void Tugas3()
{
    Console.WriteLine("--IMP Fashion (Baju)--");
    Console.Write("Masukkan Kode Baju : ");
    int kodeBaju = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kode Ukuran : ");
    string kodeUkuran = Console.ReadLine().ToUpper();

    if (kodeBaju == 1)
    {
        Console.WriteLine("Merk Baju = IMP");
        if (kodeUkuran == "S")
        {
            Console.WriteLine("Harga = 200000");
        }
        else if (kodeUkuran == "M")
        {
            Console.WriteLine("Harga = 220000");
        }
        else
        {
            Console.WriteLine("Harga = 250000");
        }
    }
    else if (kodeBaju == 2)
    {
        Console.WriteLine("Merk Baju = Prada");
        if (kodeUkuran == "S")
        {
            Console.WriteLine("Harga = 150000");
        }
        else if (kodeUkuran == "M")
        {
            Console.WriteLine("Harga = 160000");
        }
        else
        {
            Console.WriteLine("Harga = 170000");
        }
    }
    else if (kodeBaju == 3)
    {
        Console.WriteLine("Merk Baju = Gucci");
        if (kodeUkuran == "S")
        {
            Console.WriteLine("Harga = 200000");
        }
        else if (kodeUkuran == "M")
        {
            Console.WriteLine("Harga = 200000");
        }
        else
        {
            Console.WriteLine("Harga = 200000");
        }
    }
}

static void Tugas2()
{
    Console.WriteLine("--Time Conversion--");
    Console.Write("Masukkan input Time (PM/AM) : ");
    string input = Console.ReadLine();

    //Cara 1
    //Console.WriteLine(Convert.ToDateTime(input).ToString("HH:mm:ss"));

    //Cara 2
    //if (pm_am == "PM")
    //{
    //    result = string.Format("{0:00}", (jam + 12)) + ":" + string.Format("{0:00}", menit) + ":" + string.Format("{0:00}", detik);
    //}
    //else
    //{
    //    result = string.Format("{0:00}", (jam)) + ":" + string.Format("{0:00}", menit) + ":" + string.Format("{0:00}", detik);
    //}

    //Cara 3
    //result = pm_am == "PM" ? string.Format("{0:00}", (jam + 12)) + ":" + string.Format("{0:00}", menit) + ":" + string.Format("{0:00}", detik) :
    //    string.Format("{0:00}", (jam)) + ":" + string.Format("{0:00}", menit) + ":" + string.Format("{0:00}", detik);

    string PM = input.Substring(8, 2);

    string hasil = "";

    if (PM == "PM")
    {
        int jam = int.Parse(input.Substring(0, 2));
        jam += 12;
        jam = jam >= 24 ? jam - 24 : jam;
        hasil = jam.ToString().PadLeft(2, '0') + input.Substring(2, 6);
    }
    else
    {
        hasil = input.Replace("AM", "").Replace("PM", "");
    }
    Console.WriteLine(hasil);
}

static void Tugas1()
{
    Console.WriteLine("-- -5 10 -15 20 -25 30 -35 --");
    Console.Write("Masukkan input N : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai awal : ");
    int awal = int.Parse(Console.ReadLine());
    Console.Write("Masukkan kelipatan : ");
    int kelipatan = int.Parse(Console.ReadLine());

    //Jadi dinamis
    int hasil = Math.Abs(awal); //int hasil = 0;
    for (int i = 0; i < input; i++)
    {
        if(i == 0)
        {
            Console.Write($"{awal}\t");
        }
        else
        {
            hasil += kelipatan;

            if(awal >= 0)
            {
                if (i % 2 == 0)
                {
                    Console.Write($"{hasil}\t");
                }
                else
                {
                    Console.Write($"{hasil * (-1)}\t");
                }
            }
            else
            {
                if (i % 2 == 0)
                {
                    Console.Write($"{hasil * (-1)}\t");
                }
                else
                {
                    Console.Write($"{hasil}\t");
                }
            }
        }

        /*hasil += kelipatan;
        if (i % 2 == 0)
        {
            Console.Write($"{hasil}\t");
        }
        else
        {
            Console.Write($"{hasil * (-1)}\t");
        }*/

    }
}