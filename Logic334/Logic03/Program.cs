﻿
//PerulanganWhile();
//PerulanganWhile2();
//PerulanganDoWhile();
//PerulanganFor();
//Break();
//Continue();
//ForNested();
//Foreach();
//Length();
//RemoveString();
//InsertString();
//ReplaceString();
//SplitAndJoin();
//SubString();
//ContainsString();
//ToCharArray();
ConverAll();


Console.ReadKey();



static void ConverAll()
{
    Console.WriteLine("--Convert All--");
    Console.Write("Masukkan input angka (pakai koma) : ");
    string[] input = Console.ReadLine().Split(",");

    int sum = 0;

    int[] array = Array.ConvertAll(input, int.Parse);

    foreach(int i in array)
    {
        sum += i;
    }

    Console.WriteLine($"Jumlah = {sum}");
}

static void ToCharArray()
{
    Console.WriteLine("--String ToCharArray--");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();

    char[] array = kalimat.ToCharArray();

    foreach (char c in array)
    {
        Console.WriteLine(c);
    }

    Console.WriteLine();

    for(int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }
}

static void ContainsString()
{
    Console.WriteLine("--Contains String--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukkan contain : ");
    string contain = Console.ReadLine();

    if (kata.Contains(contain))
    {
        Console.WriteLine($"Kata {kata} mengandung {contain}");
    }
    else
    {
        Console.WriteLine($"Kata {kata} tidak mengandung {contain}");
    }
}

static void SubString()
{
    Console.WriteLine("--Sub String--");
    Console.Write("Masukkan kode : ");
    string kode = Console.ReadLine();
    Console.Write("Masukkan parameter 1 : ");
    int param1 = int.Parse(Console.ReadLine());
    Console.Write("Masukkan parameter 2 : ");
    int param2 = int.Parse(Console.ReadLine());

    if(param2 == 0)
    {
        Console.WriteLine($"Hasil SubString = {kode.Substring(param1)}");
    }
    else
    {
        Console.WriteLine($"Hasil SubString = {kode.Substring(param1, param2)}");
    }
}

static void SplitAndJoin()
{
    Console.WriteLine("--Split And Join--");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukkan split : ");
    string split = Console.ReadLine();

    string[] kataKata = kalimat.Split(split);

    foreach(string kata in kataKata)
    {
        Console.WriteLine(kata);
    }

    Console.WriteLine(string.Join(" + ", kataKata));

    Console.WriteLine();

    int[] deret = { 1, 2, 3, 4, 5 };
    Console.WriteLine(string.Join(" + ", deret));
}

static void ReplaceString()
{
    Console.WriteLine("--Replace String--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukkan kata yang akan di replace : ");
    string kataLama = Console.ReadLine();
    Console.Write("Masukkan kata yang baru : ");
    string kataBaru = Console.ReadLine();

    Console.WriteLine($"{kata.Replace(kataLama, kataBaru)}");
}

static void InsertString()
{
    Console.WriteLine("--Insert String--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukkan Index Insert : ");
    int index = int.Parse(Console.ReadLine());
    Console.Write("Masukkan kata yang di Insert : ");
    string insertKata = Console.ReadLine();

    Console.WriteLine($"{kata.Insert(index, insertKata)}");
}

static void RemoveString()
{
    Console.WriteLine("--Remove String--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukkan Index Remove : ");
    int index = int.Parse(Console.ReadLine());

    Console.WriteLine($"{kata.Remove(index)}");
}

static void Length()
{
    Console.WriteLine("--Length--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();

    Console.WriteLine($"Kata {kata.ToUpper()} mempunyai panjang karakter = {kata.Length}");
}

static void Foreach()
{
    int[] array = { 89, 90, 91, 92, 93 };
    int sum = 0;

    foreach(int x in array)
    {
        sum += x;
    }
    Console.WriteLine($"Jumlah = {sum}");

    Console.WriteLine();

    sum = 0;
    for(int i = 0; i < array.Length; i++)
    {
        sum += array[i];
    }
    Console.WriteLine($"Jumlah = {sum}");

}

static void ForNested()
{
    for(int i = 0; i < 3; i++)//iterasi baris
    {
        for(int j = 0; j < 3; j++)//iterasi kolom
        {
            Console.Write($"({i},{j})");
        }
        Console.WriteLine();
    }
}

static void Continue()
{
    for (int i = 0; i < 10; i++)
    {
        if (i >= 2 && i <= 7)
        {
            continue;
        }

        Console.WriteLine(i);
    }
}

static void Break()
{
    for(int i = 0; i < 10; i++)
    {
        if(i == 6)
        {
            break;
        }

        Console.WriteLine(i);
    }
}

static void PerulanganFor()
{
    Console.WriteLine("--Perulangan For--");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());

    for(int i = 0; i < input; i++)
    {
        Console.Write(i + "\t");
    }

    Console.WriteLine("\n");

    for(int i = input; i >= 0; i--)
    {
        Console.WriteLine(i);
    }
}

static void PerulanganDoWhile()
{
    Console.WriteLine("--Perulangan Do While--");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    do
    {
        Console.WriteLine(nilai);
        nilai++;

    } while (nilai < 6);
}

static void PerulanganWhile2()
{
    bool ulangi = true;

    Console.WriteLine("--Perulangan While 2--");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while (ulangi)
    {
        Console.WriteLine($"Proses ke {nilai}");
        nilai++;

        Console.Write("Ulangi proses ? (y/n) : ");
        string input = Console.ReadLine();

        if(input.ToLower() == "n")
        {
            ulangi = false;
        }
    }
}

static void PerulanganWhile()
{
    Console.WriteLine("--Perulangan While--");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while(nilai < 6)
    {
        Console.WriteLine(nilai);
        nilai++;
    }
}