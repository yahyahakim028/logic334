﻿using Logic04;

//InitializeArray();
//MengaksesElementArray();
//Array2Dimensi();
//InitializeList();
//PanggilClassStudent();
//MengaksesElementList();
//InsertList();
//RemoveList();
//IndexElementList();
//InitializeDateTime();
//ParsingDateTime();
//DateTimeProperties();
//TimeSpan();
List2Dimensi();


Console.ReadKey();



static void List2Dimensi()
{
    Console.WriteLine("--List 2 Dimensi--");
    List<List<int>> list = new List<List<int>>()
    {
        new List<int>() { 1, 2, 3 },
        new List<int>() { 4, 5, 6 },
        new List<int>() { 7, 8, 9 }
    };

    //tambah data
    list.Add(new List<int>() { 10, 11, 12 });

    //ubah data
    list[0][1] = 22; //ubah nilai 2 menjadi 22

    //hapus data
    list.RemoveAt(3); //hapus pakai index
    //list.Remove(list[3]); //hapus pakai value

    for(int i = 0; i < list.Count; i++)
    {
        for(int j = 0; j < list[i].Count; j++)
        {
            Console.Write(list[i][j] + "\t");
        }
        Console.WriteLine();
    }
}

static void TimeSpan()
{
    Console.WriteLine("--TimeSpan--");
    DateTime date1 = new DateTime(2016, 1, 10, 11, 20, 30);
    DateTime date2 = new DateTime(2016, 2, 20, 12, 25, 35);

    //Calculate the interval between the two dates
    TimeSpan interval = date2 - date1;

    Console.WriteLine("Nomor of Days : " + interval.Days);
    Console.WriteLine("Total Nomor of Days : " + interval.TotalDays);
    Console.WriteLine("Nomor of Hours : " + interval.Hours);
    Console.WriteLine("Total Nomor of Hours : " + interval.TotalHours);
    Console.WriteLine("Nomor of Minutes : " + interval.Minutes);
    Console.WriteLine("Total Nomor of Minutes : " + interval.TotalMinutes);
    Console.WriteLine("Nomor of Seconds : " + interval.Seconds);
    Console.WriteLine("Total Nomor of Seconds : " + interval.TotalSeconds);
    Console.WriteLine("Nomor of Milliseconds : " + interval.Milliseconds);
    Console.WriteLine("Total Nomor of Milliseconds : " + interval.TotalMilliseconds);
    Console.WriteLine("Ticks : " + interval.Ticks);
}

static void DateTimeProperties()
{
    Console.WriteLine("--DateTime Properties--");
    DateTime date = new DateTime(2023, 11, 1, 11, 10, 25);

    int tahun = date.Year;
    int bulan = date.Month;
    int hari = date.Day;
    int jam = date.Hour;
    int menit = date.Minute;
    int detik = date.Second;
    int weekday = (int)date.DayOfWeek;
    string hariString = date.DayOfWeek.ToString();
    string hariString2 = date.ToString("dddd");

    Console.WriteLine($"Tahun : {tahun}");
    Console.WriteLine($"Bulan : {bulan}");
    Console.WriteLine($"Hari : {hari}");
    Console.WriteLine($"Jam : {jam}");
    Console.WriteLine($"Menit : {menit}");
    Console.WriteLine($"Detik : {detik}");
    Console.WriteLine($"WeekDay : {weekday}");
    Console.WriteLine($"HariString : {hariString}");
    Console.WriteLine($"HariString2 : {hariString}");
}

static void ParsingDateTime()
{
    Console.WriteLine("--Parsing DateTime--");
    Console.Write("Masukkan DateTime (d/MM/yyyy) : ");
    string dateString = Console.ReadLine();

    try
    {
        DateTime date1 = DateTime.ParseExact(dateString, "d/MM/yyyy", null);
        Console.WriteLine(date1);
    }
    catch(Exception e)
    {
        Console.WriteLine("Format yang anda masukkan salah!");
        Console.WriteLine("Pesan error : " + e.Message);
    }
}

static void InitializeDateTime()
{
    Console.WriteLine("--Initialize DateTime--");

    DateTime dt1 = new DateTime(); //01/01/0001 00:00:00.000
    Console.WriteLine(dt1);

    DateTime dtNow = DateTime.Now; //Tgl dan waktu hari ini
    Console.WriteLine(dtNow);
    Console.WriteLine(dtNow.ToString("dddd, dd/MMMM/yyyy"));//tampilkan dengan format string 

    DateTime dt2 = new DateTime(2023, 11, 1);
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2023, 11, 1, 10, 40, 25);
    Console.WriteLine(dt3);
}

static void IndexElementList()
{
    Console.WriteLine("--Index Element List--");
    List<string> list = new List<string>();
    list.Add("1");
    list.Add("2");
    list.Add("3");
    list.Add("3");

    Console.Write("Masukkan element data : ");
    string item = Console.ReadLine();

    int index = list.IndexOf(item);

    if(index != -1)
    {
        Console.WriteLine($"Element {item} is found at index {index}");
    }
    else
    {
        Console.WriteLine($"Element {item} is not found");
    }
}

static void RemoveList()
{
    Console.WriteLine("--Remove List--");
    List<string> list = new List<string>();
    list.Add("1");
    list.Add("2");
    list.Add("3");
    list.Add("3");

    list.RemoveAt(0); //jika index tdk ada maka error
    list.Remove("3"); //ke remove hanya 1 data

    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}

static void InsertList()
{
    Console.WriteLine("--Insert List--");

    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    list.Insert(1, 4);

    for(int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}

static void MengaksesElementList()
{
    Console.WriteLine("--Mengakses Element List--");

    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    Console.WriteLine(list[0]);
    Console.WriteLine(list[1]);
    Console.WriteLine(list[2]);

    foreach(int item in list)
    {
        Console.WriteLine(item);
    }

    Console.WriteLine();

    for(int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}

static void PanggilClassStudent()
{
    Console.WriteLine("--Panggil Class Student--");

    //panggil class student/inisialisasi clas
    //Student student = new Student();

    List<Student> students = new List<Student>()
    {
        new Student() { Id = 1, Name = "John Doe" },
        new Student() { Id = 2, Name = "Jane Doe" },
        new Student() { Id = 3, Name = "Joe Doe" }
    };

    //tambah data
    Student student = new Student();
    student.Id = 4;
    student.Name = "Joko Doe";
    students.Add(student);

    students.Add(new Student() { Id = 5, Name = "Joko2 Doe" });

    Console.WriteLine($"Panjang data list Student = {students.Count}");

    //loop class pakai foreach
    foreach(Student item in students)
    {
        Console.WriteLine($"Id : {item.Id} , Name : {item.Name}");
    }

    Console.WriteLine();

    //loop class pakai for
    for (int i = 0; i < students.Count; i++)
    {
        Console.WriteLine($"Id : {students[i].Id} , Name : {students[i].Name}");
    }
}

static void InitializeList()
{
    Console.WriteLine("--Initialize List--");
    List<string> list = new List<string>()
    {
        "John Doe",
        "Jane Doe",
        "Joe Doe"
    };

    //tambah data
    list.Add("Joko Doe");

    //ubah data
    list[3] = "Joko2 Doe";

    //hapus data
    list.RemoveAt(3); //jika index tdk ada maka error
    list.Remove("Joko2 Doe");

    Console.WriteLine(string.Join(" , ", list));
}

static void Array2Dimensi()
{
    int[,] array = new int[3, 3]
    {
        { 1, 2, 3 },
        { 4, 5, 6 },
        { 7, 8, 9 }
    };

    for(int i = 0; i < array.GetLength(0); i++)
    {
        for(int j = 0; j < array.GetLength(1); j++)
        {
            Console.Write(array[i, j] + " ");
        }
        Console.WriteLine();
    }
}

static void MengaksesElementArray()
{
    Console.WriteLine("--Mengakses Element Array--");

    int[] intArray = new int[3];

    //mengisi data
    intArray[0] = 1;
    intArray[1] = 2;
    intArray[2] = 3;

    //mengambil data
    Console.WriteLine(intArray[0]);
    Console.WriteLine(intArray[1]);
    Console.WriteLine(intArray[2]);

    int[] array = { 2, 3, 4 };
    for(int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }

    string[] stringArray = { "Mahesh Chand", "Mike Gold", "Dinesh Beniwal" };
    foreach(string str in stringArray)
    {
        Console.WriteLine(str);
    }
}

static void InitializeArray()
{
    Console.WriteLine("--Initialize Array--");

    //CARA 1
    int[] array = new int[5];

    //CARA 2
    int[] array2 = new int[5] { 1, 2, 3, 4, 5 };

    //CARA 3
    int[] array3 = new int[] { 1, 2, 3, 4, 5 };

    //CARA 4
    int[] array4 = { 1, 2, 3, 4, 5 };

    //CARA 5
    int[] array5;
    array5 = new int[5] { 1, 2, 3, 4, 5 };

    Console.WriteLine(string.Join(" , ", array));
    Console.WriteLine(string.Join(" , ", array2));
    Console.WriteLine(string.Join(" , ", array3));
    Console.WriteLine(string.Join(" , ", array4));
    Console.WriteLine(string.Join(" , ", array5));
}