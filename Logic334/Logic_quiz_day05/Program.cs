﻿
//Tugas1();
//Tugas2();
//Tugas3();
//Tugas4();
//Tugas5();
//Tugas6();
//Tugas7();
//Tugas8();


Console.ReadKey();


static void Tugas8()
{
    Console.WriteLine("--Array Staircase 2 Dimensi--");
    Console.Write("Masukkan input N : ");
    int input = int.Parse(Console.ReadLine());

    for (int i = 1; i <= input; i++)
    {
        for (int j = 1; j <= input; j++)
        {
            if (i == 1 || i == input)
            {
                if (i == input)
                {
                    Console.Write($"{input - j + 1}\t");
                }
                else
                {
                    Console.Write($"{j}\t");
                }
            }
            else
            {
                if (j == 1 || j == input)
                {
                    Console.Write("*\t");
                }
                else
                {
                    Console.Write(" \t");
                }
            }
        }
        Console.WriteLine();
    }
}

static void Tugas7()
{
    Console.WriteLine("--Fibonaci 2--");
    Console.Write("Masukkan input N : ");
    int input = int.Parse(Console.ReadLine());

    int[] temp = new int[input];
    for (int i = 0; i < input; i++)
    {
        if (i <= 1)
        {
            temp[i] = 1;
        }
        else
        {
            temp[i] = temp[i - 1] + temp[i - 2];
        }
    }
    Console.WriteLine(String.Join(",", temp));
}

static void Tugas6()
{
    Console.WriteLine("-- 3 * 27 * 243 * 2187 --");
    Console.Write("Masukkan input N : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Perkalian : ");
    int perkalian = int.Parse(Console.ReadLine());

    int hasil = 0, temp = 0;
    for (int i = 0; i < input; i++)
    {
        if (i == 0)
        {
            hasil = perkalian;
            Console.Write($"{hasil}\t");
        }
        else
        {
            hasil *= perkalian;
            if (i % 2 == 0)
            {
                Console.Write($"{hasil}\t");
            }
            else
            {
                Console.Write($"*\t");
            }
        }
        temp = hasil;
    }
}

static void Tugas5()
{
    Console.WriteLine("--saya pasti bisa--");
    Console.Write("Masukkan kalimat : ");
    string[] kalimat = Console.ReadLine().Split(" ");

    //CARA SIMPLE
    //string kalimat_tes_simple = string.Join(" ", Console.ReadLine().Split(" ").Select(kata => kata.Length >= 2 ? kata.Substring(1) : kata);

    string hasil = "";
    for (int i = 0; i < kalimat.Length; i++)
    {
        char[] array = kalimat[i].ToCharArray();
        for (int j = 0; j < array.Length; j++)
        {
            if (j != 0)
            {
                hasil += array[j];
                if (j == array.Length - 1)
                {
                    hasil += " ";
                }
            }
        }
    }
    Console.WriteLine($"{hasil}");
}

static void Tugas4()
{
    Console.WriteLine("--Aku Mau Makan--");
    Console.Write("Masukkan kalimat : ");
    string[] kalimat = Console.ReadLine().Split(" ");

    string hasil = "";
    for (int i = 0; i < kalimat.Length; i++)
    {
        char[] array = kalimat[i].ToCharArray();
        for (int j = 0; j < array.Length; j++)
        {
            if (j == 0 || j == array.Length - 1)
            {
                hasil += "*";
                if (j == array.Length - 1)
                {
                    hasil += " ";
                }
            }
            else
            {
                hasil += array[j];
            }
        }
    }
    Console.WriteLine($"{hasil}");
}

static void Tugas3()
{
    Console.WriteLine("--A*u S****g K**u--");
    Console.Write("Masukkan kalimat : ");
    string[] kalimat = Console.ReadLine().Split(" ");

    string hasil = "";
    for (int i = 0; i < kalimat.Length; i++)
    {
        char[] array = kalimat[i].ToCharArray();
        for (int j = 0; j < array.Length; j++)
        {
            if (j == 0 || j == array.Length - 1)
            {
                hasil += array[j];
                if (j == array.Length - 1)
                {
                    hasil += " ";
                }
            }
            else
            {
                hasil += "*";
            }
        }
    }
    Console.WriteLine($"{hasil}");
}

static void Tugas2()
{
    Console.WriteLine("--Total Kata--");
    Console.Write("Masukkan kalimat : ");
    string[] kalimat = Console.ReadLine().Split(" ");

    for (int i = 0; i < kalimat.Length; i++)
    {
        Console.WriteLine($"Kata {i + 1} = {kalimat[i]}");
    }
    Console.WriteLine($"Total kata adalah {kalimat.Length}");
}

static void Tugas1()
{
    Console.WriteLine("--Gaji Golongan Karyawan--");
    Console.Write("Masukkan golongan : ");
    int golongan = int.Parse(Console.ReadLine());
    Console.Write("Masukkan jam kerja : ");
    int jamKerja = int.Parse(Console.ReadLine());

    double upah = 0, lembur = 0, total = 0;
    int upahGol1 = 2000, upahGol2 = 3000, upahGol3 = 4000, upahGol4 = 5000;

    if (golongan == 1)
    {
        if (jamKerja > 40)
        {
            upah = upahGol1 * 40;
            lembur = upahGol1 * (jamKerja - 40) * 1.5;
        }
        else
        {
            upah = upahGol1 * jamKerja;
        }
    }
    else if (golongan == 2)
    {
        if (jamKerja > 40)
        {
            upah = upahGol2 * 40;
            lembur = upahGol2 * (jamKerja - 40) * 1.5;
        }
        else
        {
            upah = upahGol2 * jamKerja;
        }
    }
    else if (golongan == 3)
    {
        if (jamKerja > 40)
        {
            upah = upahGol3 * 40;
            lembur = upahGol3 * (jamKerja - 40) * 1.5;
        }
        else
        {
            upah = upahGol3 * jamKerja;
        }
    }
    else if (golongan == 4)
    {
        if (jamKerja > 40)
        {
            upah = upahGol4 * 40;
            lembur = upahGol4 * (jamKerja - 40) * 1.5;
        }
        else
        {
            upah = upahGol4 * jamKerja;
        }
    }
    total = upah + lembur;

    Console.WriteLine($"Upah : {upah}");
    Console.WriteLine($"Lembur : {lembur}");
    Console.WriteLine($"Total : {total}");
}