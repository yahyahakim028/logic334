﻿
//Tugas1();
//Tugas2();
//Tugas3();
//Tugas4();
//Tugas5();
//Tugas6();

Console.ReadKey();

static void Tugas6()
{
    Console.WriteLine("--Ganjil Genap--");
    Console.Write("Masukkan nilai : ");
    int input = int.Parse(Console.ReadLine());

    if (input % 2 == 0)
    {
        Console.WriteLine($"Angka {input} adalah genap");
    }
    else
    {
        Console.WriteLine($"Angka {input} adalah ganjil");
    }
}

static void Tugas5()
{
    Console.WriteLine("--Grade Nilai--");
    Console.Write("Masukkan nilai : ");
    int input = int.Parse(Console.ReadLine());

    if (input >= 80 && input <= 100)
    {
        Console.WriteLine("Anda mendapatkan nilai Grade A");
    }
    else if (input >= 60 && input < 80)
    {
        Console.WriteLine("Anda mendapatkan nilai Grade B");
    }
    else if (input >= 0 && input < 60)
    {
        Console.WriteLine("Anda mendapatkan nilai Grade C");
    }
    else
    {
        Console.WriteLine("Anda salah memasukkan nilai");
    }
}

static void Tugas4()
{
    Console.WriteLine("--Puntung Rokok--");
    Console.Write("Masukkan jumlah puntung rokok : ");
    int input = int.Parse(Console.ReadLine());

    int hasilRangkaiRokok = input / 8;
    int sisapuntung = input % 8;

    Console.WriteLine($"Jumlah rokok yang berhasil dirangkai : {hasilRangkaiRokok}");
    Console.WriteLine($"Sisa puntung rokok : {sisapuntung}");
    Console.WriteLine($"Total Penghasilan : Rp. {hasilRangkaiRokok * 500}");
}

static void Tugas3()
{
    Console.WriteLine("--Modulus--");
    Console.Write("Masukkan nilai angka : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai pembagi : ");
    int pembagi = int.Parse(Console.ReadLine());

    if (angka % pembagi == 0)
    {
        Console.WriteLine($"Angka (variable angka) % (variable pembagi) adalah {angka % pembagi}");
    }
    else
    {
        Console.WriteLine($"Angka (variable angka) % (variable pembagi) bukan 0 melainkan hasil {angka % pembagi}");
    }
}

static void Tugas2()
{
    Console.WriteLine("--Mencari Luas dan Keliling Persegi--");
    Console.Write("Masukkan panjang sisi : ");
    double input = double.Parse(Console.ReadLine());

    double luas = input * input;
    double keliling = 4 * input;

    Console.WriteLine($"Luas Persegi : {luas}");
    Console.WriteLine($"Keliling Persegi : {keliling}");
}

static void Tugas1()
{
    Console.WriteLine("--Mencari Luas dan Keliling Lingkaran--");
    Console.Write("Masukkan panjang jari-jari : ");
    double input = double.Parse(Console.ReadLine());

    const double phi = 3.14;

    double luas = phi * input * input;
    double keliling = 2 * phi * input;

    Console.WriteLine($"Luas Lingkaran : {luas}");
    Console.WriteLine($"Keliling Lingkaran : {keliling}");
}
