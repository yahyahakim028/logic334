﻿
//Soal1();
//Soal2();
Soal3();

Console.ReadKey();

static void Soal3()
{
    Console.WriteLine("--Soal 2 (Recursive Digit)--");
    Console.Write("Masukkan input (pakai koma) : ");
    string[] input = Console.ReadLine().Replace(" ", "").Split(",");

    int hasil = 0;

    char[] array = input[0].ToCharArray();

    for (int i = 0; i < array.Length; i++)
    {
        hasil += Convert.ToInt32(array[i].ToString());
    }

    hasil *= Convert.ToInt32(input[1]);

    char[] arrayHasil = hasil.ToString().ToCharArray();

    hasil = 0;

    for (int i = 0; i < arrayHasil.Length; i++)
    {
        hasil += Convert.ToInt32(arrayHasil[i].ToString());
    }

    Console.WriteLine($"Hasil : {hasil}");
}

static void Soal2()
{
    Console.WriteLine("--Soal 1 (Invoice Penjualan)--");
    Console.Write("Masukkan start : ");
    int start = int.Parse(Console.ReadLine());
    Console.Write("Masukkan end : ");
    int end = int.Parse(Console.ReadLine());
    Console.Write("Masukkan format awal : ");
    string format = Console.ReadLine();

    DateTime date = DateTime.Now;

    for (int i = start; i <= end; i++)
    {
        Console.WriteLine($"{format}{date.ToString("ddMMyyyy")}-{i.ToString().PadLeft(5, '0')}");
    }
}

static void Soal1()
{

}