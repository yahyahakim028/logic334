﻿
//PadLeft();
Rekursif();


Console.ReadKey();


static void Rekursif()
{
    Console.WriteLine("--Rekursif Function--");
    Console.Write("Masukkan input awal : ");
    int start = int.Parse(Console.ReadLine());
    Console.Write("Masukkan input akhir : ");
    int end = int.Parse(Console.ReadLine());
    Console.Write("Masukkan type (ASC/DESC) : ");
    string type = Console.ReadLine().ToUpper();

    //panggil fungsi
    Perulangan(start, end, type);
}

static int Perulangan(int start, int end, string type)
{
    if (type == "ASC")
    {
        if (start == end)
        {
            Console.WriteLine(start);
            return 0;
        }

        Console.WriteLine(start);
        return Perulangan(start + 1, end, type);
    }
    else
    {
        if (start == end)
        {
            Console.WriteLine(end);
            return 0;
        }

        Console.WriteLine(end);
        return Perulangan(start, end - 1, type);
    }
}

static void PadLeft()
{
    Console.WriteLine("--Pad Left--");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan panjang karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Char : ");
    char chars = char.Parse(Console.ReadLine());

    //231100001 -> 2 digit tahun + 2 digit bulan + generate length

    DateTime date = DateTime.Now;

    string code = "";

    code = date.ToString("yyMM") + input.ToString().PadLeft(panjang, chars);
    //code = date.ToString("yyMM") + input.ToString().PadRight(panjang, chars);

    Console.WriteLine($"Hasil PadLeft : {code}");
}
