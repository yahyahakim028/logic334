﻿
//Tugas1();
//Tugas2();
//Tugas3();
Tugas4();
//Tugas5();
//Tugas6();
//Tugas7();
//Tugas8();
//Tugas9();
//Tugas10();


Console.ReadKey();


static void Tugas10()
{
    Console.WriteLine("--Meniup Lilin Tertinggi--");
    Console.Write("Jumlah Lilin : ");
    int jumlahLilin = int.Parse(Console.ReadLine());
    int[] arrLilin = new int[jumlahLilin];

    int lilinTertinggi = 0;
    for (int i = 0; i < jumlahLilin; i++)
    {
        Console.Write("Tinggi Lilin " + (i + 1).ToString() + " : ");
        arrLilin[i] = int.Parse(Console.ReadLine());
        if (arrLilin[i] > lilinTertinggi) { lilinTertinggi = arrLilin[i]; }

        //BISA JADI 1 FOR, YG KONDISI IF DI BAWAH BISA DIPINDAH KESINI
    }

    int banyakLilinDitiup = 0;
    for (int i = 0; i < arrLilin.Length; i++)
    {
        if (arrLilin[i] == lilinTertinggi)
        {
            banyakLilinDitiup++;
        }
    }

    Console.WriteLine("Banyaknya Lilin Tertinggi Yang Berhasil Ditiup = " + banyakLilinDitiup);
}

static void Tugas9()
{
    Console.WriteLine("--Diagonal Difference--");
    int[,] data = new int[,]
    {
                { 11, 2, 4 },
                { 4, 5, 6 },
                { 10, 8, -12 }
    };

    int[,,] data2 = new int[,,]
    {
                { {1,2,3},
                 {4,5,6},
                 {7,8,9 }
                },
                { {1,2,3},
                 {4,5,6},
                 {7,8,9 }
                },
                { {1,2,3},
                 {4,5,6},
                 {7,8,9 }
                },
    };
    int dimensi1 = data2.GetLength(0);
    int dimensi2 = data2.GetLength(1);
    int dimensi3 = data2.GetLength(2);

    int diagonal1 = 0, diagonal2 = 0;
    string strDiagonal1 = "", strDiagonal2 = "";
    for (int i = 0; i < data.GetLength(0); i++)
    {
        for (int j = 0; j < data.GetLength(1); j++)
        {
            if (i == j)
            {
                diagonal1 += data[i, j];
                strDiagonal1 += strDiagonal1 == "" ? data[i, j].ToString() : data[i, j] < 0 ? " - " + Math.Abs(data[i, j]).ToString() : " + " + data[i, j].ToString();
                //strDiagonal1 += strDiagonal1 == "" ? data[i, j].ToString() : "+" + data[i, j].ToString();
            }
            if (j == (data.GetLength(1) - 1) - i)
            {
                diagonal2 += data[i, j];
                strDiagonal2 += strDiagonal2 == "" ? data[i, j].ToString() : data[i, j] < 0 ? " - " + Math.Abs(data[i, j]).ToString() : " + " + data[i, j].ToString();
            }
        }
    }

    Console.WriteLine($"Diagonal 1 : {strDiagonal1} = {diagonal1}");
    Console.WriteLine($"Diagonal 2 : {strDiagonal2} = {diagonal2}");
    Console.WriteLine($"Perbedaan Diagonal : {diagonal1} - {diagonal2} = {diagonal1 - diagonal2}");
}

static void Tugas8()
{
    Console.WriteLine("--StairCase--");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());

    //Cara1
    for (int i = 0; i < input; i++)
    {
        for (int j = 0; j < input; j++)
        {
            if (j < input - 1 - i)
                Console.Write(" ");
            else
                Console.Write("*");
        }
        Console.WriteLine();
    }

    //Cara2
    //for (int i = 1; i <= input; i++)
    //{
    //    Console.WriteLine(new string(' ', input - i) + new string('#', i));
    //}
}

static void Tugas7()
{
    Console.WriteLine("--Elsa dan Dimas (Total Menu)--");
    Console.Write("Total Menu : ");
    int totalMenu = int.Parse(Console.ReadLine());
    Console.Write("Index Makanan Alergi : ");
    int indexMakananAlergi = int.Parse(Console.ReadLine());
    Console.Write("Harga Menu : ");
    string strHargaMenu = Console.ReadLine();
    Console.Write("Uang Elsa : ");
    int uangElsa = int.Parse(Console.ReadLine());

    int totalHargaMenu = 0;
    string[] arrHargaMenu = strHargaMenu.Split(',');
    for (int i = 0; i < arrHargaMenu.Length; i++)
    {
        if (i != indexMakananAlergi)
        {
            totalHargaMenu += int.Parse(arrHargaMenu[i]);
        }
    }

    double elsaHarusBayar = totalHargaMenu / 2;
    double sisaUangElsa = uangElsa - elsaHarusBayar;

    Console.WriteLine("Elsa harus membayar = " + elsaHarusBayar);
    if (sisaUangElsa == 0)
    {
        Console.WriteLine("Uang Pas");
    }
    else if (sisaUangElsa > 0)
    {
        Console.WriteLine("Sisa uang Elsa = " + sisaUangElsa);
    }
    else
    {
        Console.WriteLine("Kurang bayar = " + sisaUangElsa);
    }
}

static void Tugas6()
{
    Console.WriteLine("--Adenia--");
    Console.Write("Masukkan kata : ");
    string kalimat = Console.ReadLine().Replace(" ", "").ToLower();

    char[] array = kalimat.ToCharArray();

    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine($"***{array[i]}***");
    }
}

static void Tugas5()
{
    Console.WriteLine("--Vokal dan Konsonan--");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine().Replace(" ", "").ToLower();

    char[] array = kalimat.ToCharArray();

    int vokal = 0, konsonan = 0;
    for (int i = 0; i < array.Length; i++)
    {
        //if (char.IsLetter(array[i])) //a-z
        if (array[i] >= 'a' && array[i] <= 'z')
        {
            if (array[i] == 'a' || array[i] == 'i' || array[i] == 'u' || array[i] == 'e' || array[i] == 'o')
            {
                vokal++;
            }
            else
            {
                konsonan++;
            }
        }
    }
    Console.WriteLine($"Jumlah Huruf Vokal : {vokal}");
    Console.WriteLine($"Jumlah Huruf Konsonan : {konsonan}");
}

static void Tugas4()
{
    Console.WriteLine("--Tugas4 - FT1 --");
    Console.Write("Masukkan Tanggal Mulai (dd/MM/yyyy) : ");
    int[] arrTglMulai = Array.ConvertAll(Console.ReadLine().Split("/"), int.Parse);
    DateTime tglMulai = new DateTime(arrTglMulai[2], arrTglMulai[1], arrTglMulai[0]);
    Console.Write("Masukkan Lama Kelas (hari) : ");
    int lamaKelas = int.Parse(Console.ReadLine());
    Console.Write("Masukkan hari Libur (,) : ");
    int[] arrHariLibur = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

    DateTime tglSelesai = tglMulai.AddDays(-1);
    for (int i = 0; i < lamaKelas; i++)
    {
        //cek hari libur
        for (int j = 0; j < arrHariLibur.Length; j++)
        {
            if (tglSelesai.Day == arrHariLibur[j])
            {
                tglSelesai = tglSelesai.AddDays(1);

                //cek sabtu & minggu
                if (tglSelesai.DayOfWeek == DayOfWeek.Saturday)
                {
                    tglSelesai = tglSelesai.AddDays(2);
                }
            }

        }

        tglSelesai = tglSelesai.AddDays(1);

        //cek sabtu & minggu
        if (tglSelesai.DayOfWeek == DayOfWeek.Saturday)
        {
            tglSelesai = tglSelesai.AddDays(2);
        }
    }

    DateTime tgllUjianFT1 = tglSelesai.AddDays(1);

    if (tgllUjianFT1.DayOfWeek == DayOfWeek.Saturday)
    {
        tgllUjianFT1 = tgllUjianFT1.AddDays(2);
    }

    Console.WriteLine($"Kelas akan ujian FT1 pada = {tgllUjianFT1.ToString("dd/MM/yyyy")}");
}

static void Tugas3()
{
    Console.WriteLine("--Mono Pinjam Buku (Denda)--");
    Console.Write("Tgl Pinjam Buku (dd-mm-yyyy) : ");
    string tglPinjam = Console.ReadLine();
    Console.Write("Tgl Pengembalian Buku (dd-mm-yyyy) : ");
    string tglKembali = Console.ReadLine();

    int[] arrTglPinjam = Array.ConvertAll(tglPinjam.Split("-"), int.Parse);
    int[] arrTglKembali = Array.ConvertAll(tglKembali.Split("-"), int.Parse);

    DateTime dateTglPinjam = new DateTime(arrTglPinjam[2], arrTglPinjam[1], arrTglPinjam[0]);
    DateTime dateTglKembali = new DateTime(arrTglKembali[2], arrTglKembali[1], arrTglKembali[0]);

    TimeSpan ts = dateTglKembali - dateTglPinjam.AddDays(3);

    int telat = ts.Days;
    double denda = 0;
    if (telat > 0)
    {
        denda = telat * 500;
    }

    Console.WriteLine($"Telat (Hari) = {telat}");
    Console.WriteLine($"Total Denda = {denda}");
}

static void Tugas2()
{
    Ulangi:
    Console.WriteLine("--SOS--");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine().ToUpper();

    int valid = 0, invalid = 0;
    string sOSValid = "";
    if (kalimat.Length % 3 == 0)
    {
        for (int i = 0; i < kalimat.Length; i += 3)
        {
            if (kalimat.Substring(i, 3) == "SOS")
            {
                valid++;
            }
            else
            {
                invalid++;
            }
            sOSValid += "SOS";
        }
        Console.WriteLine($"Sinyal yang benar : {sOSValid}");
        Console.WriteLine($"Sinyal yang diterima signal : {kalimat}");
        Console.WriteLine($"Total sinyal salah : {invalid}");
    }
    else
    {
        Console.WriteLine("Input invalid");
    }
    goto Ulangi;
}

static void Tugas1()
{
    Console.WriteLine("--X Orang (Factorial)--");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());

    int hasil = 1;
    string str = "";
    for (int i = input; i >= 1; i--)
    {
        hasil *= i;
        str += str == "" ? i.ToString() : " * " + i.ToString();
    }

    Console.WriteLine($"{input}! = {str} = {hasil}");
    Console.WriteLine($"Ada {hasil} cara");
}