--TUGAS SQL DAY 04

--DB_HR

--create database DB_HR

--create table tb_karyawan(
--id int primary key identity(1,1),
--nip varchar(50) not null,
--nama_depan varchar(50) not null,
--nama_belakang varchar(50) not null,
--jenis_kelamin varchar(50) not null,
--agama varchar(50) not null,
--tempat_lahir varchar(50) not null,
--tgl_lahir date not null,
--alamat varchar(100) not null,
--pendidikan_terakhir varchar(50) not null,
--tgl_masuk date not null
--)

--insert into tb_karyawan
--(nip,nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terakhir,tgl_masuk)
--values
--('001','Hamidi','Samsudin','Pria','Islam','Sukabumi','1977-04-21','Jl. Sudirman No.12','S1 Teknik Mesin','2015-12-07'),
--('002','Ghandi','Wamida','Wanita','Islam','Palu','1992-01-12','Jl. Rambutan No.22','SMA Negeri 02 Palu','2014-12-01'),
--('003','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl. Veteran No.4','S1 Pendidikan Geografi','2014-01-12')

--create table tb_divisi(
--id int primary key identity(1,1),
--kd_divisi varchar(50) not null,
--nama_divisi varchar(50) not null
--)

--insert into tb_divisi
--(kd_divisi,nama_divisi)
--values
--('GD','Gudang'),
--('HRD','HRD'),
--('KU','Keuangan'),
--('UM','Umum')

--create table tb_jabatan(
--id int primary key identity(1,1),
--kd_jabatan varchar(50) not null,
--nama_jabatan varchar(50) not null,
--gaji_pokok numeric null,
--tunjangan_jabatan numeric null
--)

--insert into tb_jabatan
--(kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan)
--values
--('MGR','Manager',5500000,1500000),
--('OB','Office Boy',1900000,200000),
--('ST','Staff',3000000,750000),
--('WMGR','Wakil Manager',4000000,1200000)

--create table tb_pekerjaan(
--id int primary key identity(1,1),
--nip varchar(50) not null,
--kd_jabatan varchar(50) not null,
--kd_divisi varchar(50) not null,
--tunjangan_kinerja numeric null,
--kota_penempatan varchar(50) null
--)

--insert into tb_pekerjaan
--(nip,kd_jabatan,kd_divisi,tunjangan_kinerja,kota_penempatan)
--values
--('001','ST','KU',750000,'Cianjur'),
--('002','OB','UM',350000,'Sukabumi'),
--('003','MGR','HRD',1500000,'Sukabumi')

select * From tb_karyawan
select * From tb_divisi
select * from tb_jabatan
select * from tb_pekerjaan

--01 Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji , yang gaji + tunjangan kinerja dibawah 5juta
select a.nama_depan + ' ' + a.nama_belakang [nama_lengkap],c.nama_jabatan,c.tunjangan_jabatan + c.gaji_pokok [gaji_tunjangan] 
From tb_karyawan a
join tb_pekerjaan b on a.nip = b.nip
join tb_jabatan c on b.kd_jabatan = c.kd_jabatan
where c.gaji_pokok + b.tunjangan_kinerja < 5000000

--02 Tampilkan nama lengkap, jabatan, nama divisi, total gaji, pajak, gaji bersih, yg gendernya pria dan penempatan kerjanya diluar sukabumi. Ket: Totalgaji=(gp+tj+tk),Pajak=(5%*Totalgaji),gajibersih=(totalgaji-pajak)
select a.nama_depan + ' ' + a.nama_belakang [nama_lengkap],c.nama_jabatan,d.nama_divisi,c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja [total_gaji],
0.05 * (c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja)[pajak],
(c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja) - (0.05 * (c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja))[gaji_bersih]
From tb_karyawan a
join tb_pekerjaan b on a.nip = b.nip
join tb_jabatan c on b.kd_jabatan = c.kd_jabatan
join tb_divisi d on b.kd_divisi = d.kd_divisi
where a.jenis_kelamin = 'Pria' and b.kota_penempatan <> 'Sukabumi'

--03 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja) * 7
select a.nip,a.nama_depan + ' ' + a.nama_belakang [nama_lengkap],c.nama_jabatan,d.nama_divisi,
0.25 * ((c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja) * 7) [bonus]
From tb_karyawan a
join tb_pekerjaan b on a.nip = b.nip
join tb_jabatan c on b.kd_jabatan = c.kd_jabatan
join tb_divisi d on b.kd_divisi = d.kd_divisi

--04 Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR
select a.nip,a.nama_depan + ' ' + a.nama_belakang [nama_lengkap],c.nama_jabatan,d.nama_divisi,
(c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja) [total_gaji],
0.05 * (c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja) [infak]
From tb_karyawan a
join tb_pekerjaan b on a.nip = b.nip
join tb_jabatan c on b.kd_jabatan = c.kd_jabatan
join tb_divisi d on b.kd_divisi = d.kd_divisi
where c.kd_jabatan = 'MGR'

--05 Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1
select a.nip,a.nama_depan + ' ' + a.nama_belakang [nama_lengkap],c.nama_jabatan,a.pendidikan_terakhir,2000000 [tunjangan_pendidikan],
(c.tunjangan_jabatan + c.gaji_pokok + 2000000) [total_gaji]
From tb_karyawan a
join tb_pekerjaan b on a.nip = b.nip
join tb_jabatan c on b.kd_jabatan = c.kd_jabatan
where a.pendidikan_terakhir like 'S1%'
order by a.nip

--06 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus. Ket: MgrBonus=(25%*totalgaji*7),STBonus=(25%*totalgaji*5),OtherBonus=(25%*totalgaji*2)
select a.nip,a.nama_depan + ' ' + a.nama_belakang [nama_lengkap],c.nama_jabatan,d.nama_divisi,
case  c.kd_jabatan
when 'MGR' then 0.25 * ((c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja) * 7) 
when 'ST' then 0.25 * ((c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja) * 5) 
else 0.25 * ((c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja) * 2) 
end [bonus]
From tb_karyawan a
join tb_pekerjaan b on a.nip = b.nip
join tb_jabatan c on b.kd_jabatan = c.kd_jabatan
join tb_divisi d on b.kd_divisi = d.kd_divisi

--07 Buatlah kolom nip pada table karyawan sebagai kolom unique
alter table tb_karyawan add constraint unique_nip unique (nip)

--08 Buatlah kolom nip pada table karyawan sebagai index
create index index_nip on tb_karyawan (nip)
--drop index index_nip on tb_karyawan

--09 Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang di awali dengan huruf W
select 
case when nama_belakang like 'w%' then nama_depan + ' ' + upper(nama_belakang) else nama_depan + ' ' + nama_belakang end [nama_lengkap]
From tb_karyawan
where nama_belakang like 'w%'

--10 Perusahaan akan memberikan bonus sebanyak 10% dari total gaji bagi karyawan yg sudah join di peruashaan diatas sama dengan  8 tahun, Tampilkan nip, nama lengkap, jabatan, nama divisi, total gaji , bonus, lama bekerja (total gaji = Gaji_pokok+Tunjangan_jabatan+Tunjangan_kinerja )
select a.nip,a.nama_depan + ' ' + a.nama_belakang [nama_lengkap],a.tgl_masuk,c.nama_jabatan,d.nama_divisi,
(c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja) [total_gaji],
0.1 * (c.tunjangan_jabatan + c.gaji_pokok + b.tunjangan_kinerja) [bonus],
FLOOR(datediff(day,a.tgl_masuk,getdate()) / 365.25) [lama_bekerja]
From tb_karyawan a
join tb_pekerjaan b on a.nip = b.nip
join tb_jabatan c on b.kd_jabatan = c.kd_jabatan
join tb_divisi d on b.kd_divisi = d.kd_divisi
where FLOOR(datediff(day,a.tgl_masuk,getdate()) / 365.25) >= 8
order by a.nip



