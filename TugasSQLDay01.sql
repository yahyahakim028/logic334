--TUGAS SQL DAY 01
--create database DBPenerbit

--create table tblPengarang(
--ID int primary key identity(1,1),
--Kd_Pengarang varchar(7) not null,
--Nama varchar(30) not null,
--Alamat varchar(80) not null,
--Kota varchar(15) not null,
--Kelamin varchar(1) not null
--)

--insert into tblPengarang
--(Kd_Pengarang,Nama,Alamat,Kota,Kelamin)
--values
--('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
--('P0002','Rian','Jl. Solo 123','Yogya','P'),
--('P0003','Suwadi','Jl. Semangka 13','Bandung','P'),
--('P0004','Siti','Jl. Durian 15','Solo','W'),
--('P0005','Amir','Jl. Gajah 33','Kudus','P'),
--('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
--('P0007','Jaja','Jl. Singa 7','Bandung','P'),
--('P0008','Saman','Jl. Naga 12','Yogya','P'),
--('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
--('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')

--create table tblGaji(
--ID int primary key identity(1,1),
--Kd_Pengarang varchar(7) not null,
--Nama varchar(30) not null,
--Gaji decimal(18,4) not null
--)

--insert into tblGaji
--(Kd_Pengarang,Nama,Gaji)
--values
--('P0002','Rian',600000),
--('P0005','Amir',700000),
--('P0004','Siti',500000),
--('P0003','Suwadi',1000000),
--('P0010','Fatmawati',600000),
--('P0008','Saman',750000)

------------------------------------------JAWABAN SOAL---------------

--Soal01
select count(Kd_Pengarang) as [Jumlah Pengarang] from tblPengarang
--Soal02
select count(Kd_Pengarang) as [Jumlah Pengarang],Kelamin from tblPengarang group by Kelamin 
--Soal03
select count(Kota) as [Jumlah Kota],Kota from tblPengarang group by Kota 
--Soal04
select count(Kota) as [Jumlah Kota],Kota from tblPengarang group by Kota having count(Kota) > 1
--Soal05
select min(Kd_Pengarang)[KodeTerkecil],max(Kd_Pengarang)[KodeTerbesar] from tblpengarang
--Soal06
select min(Gaji) as GajiTerendah, max(Gaji) as GajiTertinggi From tblgaji
--Soal07
select Gaji From tblgaji where Gaji > 600000
--Soal08
select sum(Gaji) as JumlahGaji From tblgaji
--Soal09
select sum(b.Gaji) as Gaji, a.Kota From tblpengarang a
inner join tblgaji b on a.kd_pengarang = b.kd_pengarang
group by a.Kota
--Soal10
select * From tblpengarang where kd_pengarang > 'P0003' and kd_pengarang < 'P0006'
--Soal11
select * From tblpengarang where kota = 'Yogya' or kota = 'Solo' or kota = 'Magelang' --kota in ('Yogya','Solo','Magelang')
--Soal12
select * From tblpengarang where kota != 'Yogya'
--Soal13-A
select * From tblpengarang where nama like 'a%'
--Soal13-B
select * From tblpengarang where nama like '%i'
--Soal13-C
select * From tblpengarang where nama like '__a%'
--Soal13-D
select * From tblpengarang where nama not like '%n'
--Soal14
select * From tblpengarang a
inner join tblgaji b on a.kd_pengarang = b.kd_pengarang
--Soal15
select a.Kota,b.Gaji From tblpengarang a
inner join tblgaji b on a.kd_pengarang = b.kd_pengarang
where b.gaji < 1000000
--Soal16
alter table tblpengarang alter column Kelamin varchar(10) not null
--Soal17
alter table tblpengarang add Gelar varchar(12) not null
--Soal18
update tblpengarang set alamat='Jl. Cendrawasih 65',kota='Pekanbaru' where nama='Rian'
--Soal19
create view vwPengarang as
select a.Kd_Pengarang,a.Nama,a.Kota,b.Gaji from tblpengarang a
full join tblgaji b on a.Kd_Pengarang = b.Kd_Pengarang

--select * From vwPengarang
