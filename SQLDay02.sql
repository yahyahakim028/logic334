
--SQLDay02

--in
select * from mahasiswa where name = 'tunggul' or name = 'irvan'
select * from mahasiswa where name in('tunggul','irvan')

--min max count
select * from mahasiswa
select min(id) from mahasiswa
select max(id) from mahasiswa
select count(id) from mahasiswa where name = 'banu'
select top 1 id from mahasiswa order by id asc
select top 1 id from mahasiswa order by id desc

--add column nilai pada tabel mahasiswa
alter table mahasiswa add nilai int
--update column nilai
update mahasiswa set nilai = 80 where id = 1
update mahasiswa set nilai = 80 where id = 2
update mahasiswa set nilai = 70 where id = 3
update mahasiswa set nilai = 100 where id = 4
update mahasiswa set nilai = 70 where id = 5
update mahasiswa set nilai = 50 where id = 6
update mahasiswa set nilai = 40 where id = 8

--avg sum
select avg(nilai) from mahasiswa
select sum(nilai) from mahasiswa

--inner join atau join
select * from mahasiswa
select * from biodata

select *
from mahasiswa mhs
inner join biodata bio on mhs.id = bio.mahasiswa_id

--left join
select *
from mahasiswa mhs
left join biodata bio on mhs.id = bio.mahasiswa_id
where bio.id is null

--right join
select *
from mahasiswa mhs
right join biodata bio on mhs.id = bio.mahasiswa_id
where mhs.id is null

--distinct
select distinct name from mahasiswa

--group by
select name from mahasiswa group by name

--substring
select SUBSTRING('SQL Tutorial',1,3)

--charindex
select charindex('t','Customer')

--datalength
select DATALENGTH('W3Schools.com')

--cast & convert
select cast(10 as decimal(18,4))
select cast('10' as int)
select cast(10.65 as int)
select cast('2023-03-16' as datetime)
select convert(datetime, '2023-03-16')
select convert(decimal(18,4), 10)

--case when
select name,nilai,
case 
when nilai >= 80 then 'A'
when nilai >= 60 then 'B'
else 'C'
end
as grade
from mahasiswa

--concat
select CONCAT('SQL','is','fun!')
select 'SQL' + 'is' + 'fun!'
select CONCAT('Nama : ', name) from mahasiswa

--operator aritmatika
create table penjualan(
id int primary key identity(1,1),
name varchar(50) not null,
harga decimal(18,0) not null
)

insert into penjualan(name,harga)
values
('Indomie',1500),
('Close-Up',3500),
('Pepsodent',3000),
('Brush Formula',2500),
('Roti Manis',1000),
('Gula',3500),
('Sarden',4500),
('Rokok Sampurna',11000),
('Rokok 234',11000)

select name,harga,harga * 100 [harga * 100] from penjualan

--getdate
select GETDATE()

--day month year
select DAY('2023-11-10')
select MONTH('2023-11-10')
select YEAR(GETDATE())

--dateadd
select DATEADD(year,1,'2023-11-10')
select DATEADD(month,3,'2023-11-10')
select DATEADD(day,5,'2023-11-10')
select DATEADD(hour,5,GETDATE())

--datediff
select DATEDIFF(year, '2023-11-10', '2024-01-10')
select DATEDIFF(month, '2023-11-10', '2024-01-10')
select DATEDIFF(day, '2023-11-10', DATEADD(day,5, GETDATE()) )

--sub query
select * 
from mahasiswa mhs
left join biodata bio on mhs.id = bio.mahasiswa_id
where mhs.nilai = (select max(nilai) from mahasiswa)

insert into mahasiswa_new(name,address,email)
select name,address,email from mahasiswa

--create view
create view vw_mahasiswa 
as
select name from mahasiswa where name = 'banu'

--alter view
alter view vw_mahasiswa 
as
select name,address,nilai from mahasiswa where name != 'banu'

--drop view
drop view vw_mahasiswa 

--select view
select * from vw_mahasiswa

--create index
create index index_nameemail on mahasiswa(name,email)

--create unique index
create unique index  index_id on mahasiswa(id)

--drop index
drop index index_id on mahasiswa


--primary key
create table coba(id int not null, nama varchar(50) not null)
alter table coba add constraint pk_idnama primary key(id,nama)

--drop primary key
alter table coba drop constraint pk_idnama

--unique key
alter table coba add constraint unique_nama unique(nama)

--drop unique key
alter table coba drop constraint unique_nama

--foreign key
alter table biodata 
add constraint fk_mahasiswa_id foreign key(mahasiswa_id) references mahasiswa(id)

--drop unique key
alter table biodata drop constraint fk_mahasiswa_id


-----------
--union (menggabungkan hasil dari dua atau lebih query dari SELECT)


--MISSION COMPLETED ***

