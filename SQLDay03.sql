
--SQLDay03

--STORED PROCEDURE(SP)
--create stored procedure (SP)
create procedure sp_RetieveMahasiswa
	--parameter disini
as
begin
	select name,address,email,nilai from mahasiswa
end

--run / exec procedure
exec sp_RetieveMahasiswa


--edit / alter strored procedure
alter procedure sp_RetieveMahasiswa
	--parameter disini
	@id int,
	@nama varchar(50)
as
begin
	select name,address,email,nilai from mahasiswa
	where id = @id and name = @nama
end

--run / exec procedure
exec sp_RetieveMahasiswa 1,'haikal'


--FUNCTION
--create function
create function fn_total_mahasiswa
(
	@id int
)
RETURNS INT
BEGIN
	DECLARE @hasil int
	SELECT @hasil = count(id) from mahasiswa where id = @id
	return @hasil
END

--run function
select dbo.fn_total_mahasiswa(1)

--edit / alter function
alter function fn_total_mahasiswa
(
	@id int,
	@nama varchar(50)
)
RETURNS INT
BEGIN
	DECLARE @hasil int
	SELECT @hasil = count(id) from mahasiswa where id = @id and name = @nama
	return @hasil
END

--run function
select dbo.fn_total_mahasiswa(1,'haikal')


--coba select function mahasiswa
select mhs.id,mhs.name, dbo.fn_total_mahasiswa(mhs.id, mhs.name)
from mahasiswa mhs
join biodata bio on bio.mahasiswa_id = mhs.id


--truncate table (delete dan reset isi table)
truncate table [namatable]



--------------------------
--AGE / UMUR
select FLOOR(DATEDIFF(dd,'2010-11-12',getdate()) / 365.25) as tahun

DECLARE @date datetime, @tmpdate datetime, @years int, @months int, @days int
SELECT @date = '2010-11-12'

SELECT @tmpdate = @date

SELECT @years = DATEDIFF(yy, @tmpdate, GETDATE()) - CASE WHEN (MONTH(@date) > MONTH(GETDATE())) OR (MONTH(@date) = MONTH(GETDATE()) AND DAY(@date) > DAY(GETDATE())) THEN 1 ELSE 0 END
SELECT @tmpdate = DATEADD(yy, @years, @tmpdate)
SELECT @months = DATEDIFF(m, @tmpdate, GETDATE()) - CASE WHEN DAY(@date) > DAY(GETDATE()) THEN 1 ELSE 0 END
SELECT @tmpdate = DATEADD(m, @months, @tmpdate)
SELECT @days = DATEDIFF(d, @tmpdate, GETDATE())

SELECT @years, @months, @days
--------------------------
