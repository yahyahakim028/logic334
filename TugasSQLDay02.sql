--TUGAS SQL DAY 02

--create database DB_Entertainer

--create table artis(
--kd_artis varchar(100) primary key,
--nm_artis varchar(100) not null,
--jk varchar(100) not null,
--bayaran bigint,
--award int,
--negara varchar(100)
--)

--insert into artis
--values
--('A001','ROBERT DOWNEY JR','PRIA',3000000000,2,'AS'),
--('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
--('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
--('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
--('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')


--create table film(
--kd_film varchar(10) primary key,
--nm_film varchar(55) not null,
--genre varchar(55) not null,
--artis varchar(55) not null,
--produser varchar(55) not null,
--pendapatan bigint not null,
--nominasi int not null
--)

--insert into film
--values
--('F001','IRON MAN','G001','A001','PD01',2000000000,3),
--('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
--('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
--('F004','AVENGER CIVIL WAR','G001','A001','PD01',2000000000,1),
--('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
--('F006','THE RAID','G001','A004','PD03',800000000,5),
--('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
--('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
--('F009','POLICE STORY','G001','A003','PD02',700000000,3),
--('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
--('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
--('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
--('F013','KUNGFU PANDA','G003','A003','PD05',923000000,5)

--create table produser(
--kd_produser varchar(50) primary key,
--nm_produser varchar(50) not null,
--international varchar(50) not  null
--)

--insert into produser
--values
--('PD01','MARVEL','YA'),
--('PD02','HONGKONG CINEMA','YA'),
--('PD03','RAPI FILM','TIDAK'),
--('PD04','PARKIT','TIDAK'),
--('PD05','PARAMOUNT PICTURE','YA')

--create table negara(
--kd_negara varchar(100) primary key,
--nm_negara varchar(100) not null
--)

--insert into negara
--values
--('AS','AMERIKA SERIKAT'),
--('HK','HONGKONG'),
--('ID','INDONESIA'),
--('IN','INDIA')

--create table genre(
--kd_genre varchar(50) primary key,
--nm_genre varchar(50) not null
--)

--insert into genre
--values
--('G001','ACTION'),
--('G002','HORROR'),
--('G003','COMEDY'),
--('G004','DRAMA'),
--('G005','THRILLER'),
--('G006','FICTION')

--------------------------------------------

--Soal-01 Menampilkan jumlah pendapatan produser marvel secara keseluruhan
select pr.nm_produser,SUM(fi.pendapatan)[pendapatan] From film fi
join produser pr on fi.produser = pr.kd_produser
where pr.nm_produser = 'MARVEL'
group by pr.nm_produser

--Soal-02 Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
select nm_film,nominasi From film
where nominasi = 0

--Soal-03 Menampilkan nama film yang huruf depannya 'p' (ada 2 cara: pake like atau substring)
select nm_film From film
where --nm_film like 'p%' or 
SUBSTRING(nm_film,1,1) = 'p'

--Soal-04 Menampilkan nama film yang huruf terakhir 'y'
select nm_film From film
where --nm_film like '%y' or
SUBSTRING(nm_film,DATALENGTH(nm_film),1) = 'y'

--Soal-05 Menampilkan nama film yang mengandung huruf 'd'
select nm_film From film
where nm_film like '%d%'

--Soal-06 Menampilkan nama film dan artis
select nm_film,nm_artis From film fi
join artis ar on fi.artis = ar.kd_artis

--Soal-07 Menampilkan nama film yang artisnya berasal dari negara hongkong
select nm_film,negara From film fi
join artis ar on fi.artis = ar.kd_artis
where ar.negara = 'HK'

--Soal-08 Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'
select nm_film,nm_negara From film fi
join artis ar on fi.artis = ar.kd_artis
join negara ne on ar.negara = ne.kd_negara
where ne.nm_negara not like '%o%'

--Soal-09 Menampilkan nama artis yang tidak pernah bermain film 
--(ada 2 cara: pake left join atau right join)
select nm_artis from artis ar
left join film fi on ar.kd_artis = fi.artis
where fi.nm_film is null

select nm_artis from film fi
right join artis ar on ar.kd_artis = fi.artis
where fi.nm_film is null

--Soal-10 Menampilkan nama artis yang bermain film dengan genre drama (ada 2 cara)
select nm_artis,nm_genre from artis ar
join film fi on fi.artis = ar.kd_artis
join genre ge on fi.genre = ge.kd_genre
where ge.nm_genre = 'DRAMA'

select nm_artis,nm_genre from film fi
join artis ar on fi.artis = ar.kd_artis
join genre ge on fi.genre = ge.kd_genre
where ge.nm_genre = 'DRAMA'

--Soal-11 Menampilkan nama artis yang bermain film dengan genre Action (ada 2 cara)
select distinct nm_artis,nm_genre from artis ar
join film fi on fi.artis = ar.kd_artis
join genre ge on fi.genre = ge.kd_genre
where ge.nm_genre = 'ACTION'

select distinct nm_artis,nm_genre from film fi
join artis ar on fi.artis = ar.kd_artis
join genre ge on fi.genre = ge.kd_genre
where ge.nm_genre = 'ACTION'

--Soal-12 Menampilkan data negara dengan jumlah filmnya
select kd_negara,nm_negara,count(fi.kd_film)[jumlah_film] from artis ar
join film fi on fi.artis = ar.kd_artis
right join negara ne on ar.negara = ne.kd_negara --pake right join
group by kd_negara,nm_negara

--Soal-13 Menampilkan nama film yang skala internasional
select nm_film from film fi
join produser pr on fi.produser = pr.kd_produser
where pr.international = 'YA'

--Soal-14 Menampilkan jumlah film dari masing2 produser
select nm_produser,count(fi.kd_film)[jumlah_film] from film fi
right join produser pr on fi.produser = pr.kd_produser
group by nm_produser


