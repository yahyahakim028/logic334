--SQLDay01

--DDL (Data Definition Language)

--CREATE
--create database
create database db_kampus

use db_kampus

--drop database
drop database db_kampus

--create table
create table mahasiswa(
id bigint primary key identity(1,1),
name varchar(50) not null,
address varchar(50) not null,
email varchar(255) null
)

--create view
create view vw_mahasiswa
as select * from mahasiswa

--select view
select * from vw_mahasiswa

--ALTER
--add column
alter table mahasiswa add nomor_hp varchar(100) not null

--drop column
alter table mahasiswa drop column nomor_hp

--alter column
alter table mahasiswa alter column email varchar(100) not null


--DROP
--drop database 
drop database [nama_database]

--drop table
drop table [nama_table]

--drop view
drop view [nama_view]


--DML (Data Manipulation Language)

--insert
insert into mahasiswa(name,address,email) 
values
('Haikal','Kuningan','haikal5nsah@gmail.com'),
('Imam','Bekasi','imamassidqi@gmail.com'),
('Irvan','Jakarta','aliirvan122@gmail.com'),
('rezky','Cengkareng','rezkyfajri08@gmail.com'),
('Tunggul','Semarang','tunggulyudhaputra5@gmail.com'),
('Shabrina','Palembang','shabrinaputrif1604@gmail.com')

--select
select id,name,address,email from mahasiswa

--update
update mahasiswa set name = 'Haikal Limansyah' where id = 1

--delete
delete mahasiswa where id = 1


--create table biodata
create table biodata(
id bigint primary key identity(1,1),
mahasiswa_id bigint null,
tgl_lahir date null,
gender varchar(10) null
)

select * from biodata

insert into biodata(mahasiswa_id,tgl_lahir,gender)
values
(1, '2020-06-10','Pria'),
(2, '2021-07-10','Pria'),
(3, '2022-08-10','Wanita')

--JOIN (ada AND , OR , dan NOT)
select mhs.id as id_mahasiswa, mhs.name as nama_mahasiswa, mhs.address as alamat,
mhs.email as email, bio.tgl_lahir as tanggal_lahir, bio.gender as gender
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.id = 2 and mhs.name = 'imam' or not mhs.name = 'irvan' and mhs.id = 3

--order by (asc atau desc)
select * from biodata order by tgl_lahir desc, gender asc

--top
select top 1 mahasiswa_id from biodata order by mahasiswa_id desc

--between
select * from biodata where mahasiswa_id between 2 and 3
select * from biodata where mahasiswa_id >= 2 and mahasiswa_id <= 3
select * from biodata where tgl_lahir between '2019-01-01' and '2021-01-01'

--like
select * from mahasiswa
where
--name like 'i%' --awalan i
--name like '%a' --akhiran a
--name like '%ai%' --mengandung ai (bebas/contains)
--name like '_a%' --karakter keduanya a dan length minimal 2
--name like 'i__%' --karakter pertamanya i dan length minimal 3
name like 'i%n' --awalan i dan akhiran n

--group by
select name from mahasiswa group by name

--aggregate function(SUM , COUNT , AVG , MIN , MAX)
select sum(id),name,address from mahasiswa group by name,address

--having
select sum(id) as jumlah,name
from mahasiswa
where name = 'tunggul'
group by name
having sum(id) >= 13
order by jumlah desc
